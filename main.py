import twitter
import argparse
import mongodb
import text_processing
import statistics
import plotting

def check_positive(value):
    ivalue = int(value)
    if ivalue < 0:
         raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue

parser = argparse.ArgumentParser(description='Does text mining from tweets.')
parser.add_argument('-p', '--place',  dest='place', type=int, default=23424977,
                   help='The WOEID of the country from which to receive trending topics. Default is USA(23424977).')
parser.add_argument('-a', '--append', dest='append', action='store_true',
                   help='Runs the program in append mode. If the database is not empty, it will use the trends saved from a previous session, and download more tweets for those trends')
parser.add_argument('-n', '--number', dest='number', type=check_positive, default=1500,
                   help='The number of tweets to receive. Default is 1500.')
parser.add_argument('-t', '--trends_number', metavar="[1-10]", dest='trend_num', type=int, default=5, choices=range(1,11),
                   help='The number of trends from which to download tweets, default is 5.')
parser.add_argument('--plot', dest='plot', action='store_true',
                   help='Runs the program in plot mode. All tweets and statistics data are taken locally, use only if you know your database is ready')

#parsing the arguments
args = parser.parse_args()
if args.plot:
	args.number = 0
	args.append = True
#trickery for append and plot mode :)
args.append = args.append and not mongodb.check_empty()
args.plot = args.append
#Top 5 trends from args.place
if args.append:
	trends = mongodb.trends_to_list()
	place = mongodb.get_place()
else:
	place = args.place
	trends = twitter.get_trends(place, args.trend_num)
	#database is deleted every time, except if we run in append mode
	mongodb.init_database(place, trends)

print("Top", len(trends), "trends for WOEID", place,": ",', '.join(trends))
#saving a list of lists of tweets, 1500 for each keyword (final array should be [5][1500])
tweets = []
if args.number > 0:
	for i in range(len(trends)):
		print("Receiving tweets for \""+trends[i]+"\" keyword:")
		tweets.append(twitter.find_tweets(trends[i], args.number))
		#save the tweets into mongodb
		mongodb.save_tweets(tweets[i], i)
if args.append:
	tweets = mongodb.get_all_tweets(len(trends))
#make all the statistics stuff
sentiments, frequencies, frequencies_with_stopwords = statistics.tweet_statistics(tweets, trends, args.plot)
if args.plot:
	users_data = mongodb.get_user_data(len(trends))
else:
	users_data = statistics.user_statistics(len(trends))
# plotting function here - should save image files, not pop up windows
plotting.gotta_plot_them_all(frequencies,frequencies_with_stopwords,sentiments,users_data,trends)
