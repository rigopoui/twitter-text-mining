<h2>Text mining project using the twitter API.</h2>

For the execution of the program, the following libraries must be installed for python 3: argparse, tweepy, json, sys, time, random, pymongo, bson, string, nltk, requests, operator, matplotlib, numpy, scipy, pathlib

Some of these are core python libs, don't have to be installed explicitly (json, sys, time, random, operator, requests, string)

mongod must be running in order for the program to work (sudo mongod for linux)

In the same directory as main.py you need to have a config.py file that contains your keys for accesing the twitter API. The file should look like this: 

	consumer_key = '4Fsdfijuji2WD1W0OkroT'
	consumer_secret = 'aAU48w7yhiYkKIdfk9dsfju2zl9KEFpeUe6'
	access_token = '9382987182731936-vcqJBsdlpas9dk2m1x45z'      <-- example keys - not real
	access_token_secret = 'at1ksj9d2nja82lpnigaEq9ey'
To get these keys you need to have your own twitter account!

Acceptable arguments are:

	-a --append Runs the program in append mode, downloading more tweets for 
    the same topic as the existing tweets
	-p --place Sets the place/country from which to get trending topics
	-n --number Sets the number of tweets to download from each topic
	-t --trends_number Sets the number of trends for which to download tweets
	--plot runs the program in plot mode, just computes statistics and plots 
    diagrams, you need to have run the program in normal or append mode at least once, 
    so all your data is in place in the DB

Diagrams are saved in the "images" folder, same directory as main.py
