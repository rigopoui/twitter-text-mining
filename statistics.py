import requests
import mongodb
import text_processing
import operator
import twitter

#gets the sentiment of a tweet's text, using the text-processing.com api
def compute_sentiment(tokens):
	text = ''.join(word+' ' for word in tokens)
	response = requests.post('http://text-processing.com/api/sentiment/', data = {"text": text})
	if response.status_code == 400:
		return { "label": "neutral","probability": { "pos": 0.00,"neg": 0.00,"neutral": 1.0}}
	return response.json()


#computes the statistics from the tweets (4th task)
def tweet_statistics(tweets, trends, plot):
	#processing tweets, calculating sentiment and adding the info as extra properties in tweet data, in mongodb
	proxy_list = 0#RequestProxy()
	frequencies = []
	frequencies_with_stopwords = []
	sentiments = []
	for i in range(0, len(trends)):
		frequencies.append({})
		frequencies_with_stopwords.append({})
		sentiments.append({})
		for j in range(0, len(tweets[i])):
			tweet_id = tweets[i][j]['id']
			prepared_text = text_processing.prepare_tweet(tweets[i][j], trends[i])
			prepared_text_with_stopwords = text_processing.prepare_tweet_include_stopwords(tweets[i][j], trends[i])
			#calculating frequencies!
			for word in prepared_text:
				if word in frequencies[i]:
					frequencies[i][word] += 1
				else:
					frequencies[i][word] = 1

			for word in prepared_text_with_stopwords:
				if word in frequencies_with_stopwords[i]:
					frequencies_with_stopwords[i][word] += 1
				else:
					frequencies_with_stopwords[i][word] = 1
			#calculating sentiments for each tweet
			if plot:
				sentiment = mongodb.get_sentiment(i, tweet_id)
			else:
				sentiment = compute_sentiment(prepared_text)
				mongodb.save_sentiment(i, tweet_id, sentiment)
			if j == 0:
				sentiments[i]["positive_probability"] = sentiment["probability"]["pos"]
				sentiments[i]["negative_probability"] = sentiment["probability"]["neg"]
				sentiments[i]["neutral_probability"] = sentiment["probability"]["neutral"]
			else:
				sentiments[i]["positive_probability"] += sentiment["probability"]["pos"]
				sentiments[i]["negative_probability"] += sentiment["probability"]["neg"]
				sentiments[i]["neutral_probability"] += sentiment["probability"]["neutral"]
		#averaging the sentiments for each topic
		sentiments[i]["positive_probability"] /= len(tweets[i])
		sentiments[i]["negative_probability"] /= len(tweets[i])
		sentiments[i]["neutral_probability"] /= len(tweets[i])

		frequencies_sorted = sorted(frequencies[i].items(), key=operator.itemgetter(1), reverse=True)
		frequencies_with_stopwords_sorted = sorted(frequencies_with_stopwords[i].items(), key=operator.itemgetter(1), reverse=True)
		frequencies[i] = frequencies_sorted
		frequencies_with_stopwords[i] = frequencies_with_stopwords_sorted
	return (sentiments, frequencies, frequencies_with_stopwords)

#computes the statistics for the users (5th task)
def user_statistics(trends_num):
	users_data = []
	for i in range(trends_num):
		cursor = mongodb.group_sentiments_by_user(i)
		user_data = []
		previous_id = -1
		count = 0
		neg = 0
		pos = 0
		neut = 0
		for doc  in cursor:
			current_id = doc['_id']['user_id']
			if not 'neg' in doc['_id'] or not 'pos' in doc['_id'] or not 'neutral' in doc['_id']:
				continue
			if previous_id == current_id:
				neg += doc['_id']['neg']
				pos += doc['_id']['pos']
				neutral += doc['_id']['neutral']
				count += 1
			else:
				if previous_id != -1: 
					followers, friends = twitter.user_followers_friends(previous_id)
					#special case, for if someone is followed by no one, then the ratio becomes zero
					if friends == 0:
						friends = 1
						followers = 0
					user_data.append({'user_id' : previous_id, 
						'positive_probability' : pos/count, 
						'negative_probability' : neg/count,
						'neutral_probability' : neutral/count,
						'followers_friends_ratio' : followers/friends})
				previous_id = current_id
				neg = doc['_id']['neg']
				pos = doc['_id']['pos']
				neutral = doc['_id']['neutral']
				count = 1
		followers, friends = twitter.user_followers_friends(previous_id)
		#special case, for if someone is followed by no one, then the ratio becomes zero
		if friends == 0:
			friends = 1
			followers = 0
		user_data.append({'user_id' : previous_id, 
						'positive_probability' : pos/count, 
						'negative_probability' : neg/count,
						'neutral_probability' : neutral/count,
						'followers_friends_ratio' : followers/friends})
		mongodb.save_user_data(user_data, i)
		users_data.append(user_data)
	return users_data