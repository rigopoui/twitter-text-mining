import tweepy
import config #don't forget your config.py file ;)
import json
import sys
from time import sleep
import random

# OAuth process, using the keys and tokens
auth = tweepy.OAuthHandler(config.consumer_key, config.consumer_secret)
auth.set_access_token(config.access_token, config.access_token_secret)

# Creation of the actual interface, using authentication
api = tweepy.API(auth)

class twitterListener(tweepy.StreamListener):
	#what to do when receiving data through the listener
	def on_data(self, data):
		#convert data string into json, dict object
		self.tweet = json.loads(data)
		#checking for retweets and excluding them - retweeted_status normally holds the original tweet in a retweet
		if not 'retweeted_status' in self.tweet and 'id' in self.tweet and 'id' in self.tweet['user']:
			self.tweetList.append(self.tweet)
			self.counter += 1
			#just printing some stuff, without ending in newline
			print("\r", "\bTweets received: ",str(self.counter),"/",str(self.limit), end="")
			sys.stdout.flush()
		#checking for number of tweets we are saving
		if self.counter < self.limit:
			return True
		else:
			print("")
			return False

	#what to do when receiving some error code when downloading tweets
	def on_error(self, status_code):
		print("\nError, code:", str(status_code)) #debugging purposes
	
	#what to do when the listener is created	
	def __init__(self, number):
		super().__init__()
		self.counter = 0
		self.limit = number
		self.tweet={}
		self.tweetList=[]
		sleep(random.randint(6,16)/10) #optional, sleep between switching trends, good for avoiding error code 420
		print("Tweets received: ",str(self.counter),"/",str(self.limit), end="")

#finds a specific number of tweets which contain the keyword, and returns them in a list 
def find_tweets(keyword, number):
	stream = tweepy.Stream(auth, twitterListener(number))
	#tracking for keyword, and only english tweets
	stream.filter(track = [keyword], languages = ["en"])
	return stream.listener.tweetList

#gets the 5 trending topics from a place
def get_trends(place, trend_num):
	trends = api.trends_place(place)
	trend_names = list([trend['name'] for trend in trends[0]['trends']])
	return trend_names[0:trend_num]

#based on user ID, returns the number of followers and following of this user
def user_followers_friends(user_id):
	#try catch for throttling error and for user not existing(dunno why this happened, maybe the user was deleted during execution? lol)
	while True:
		try:
			user = api.get_user(user_id = user_id)
			break
		except tweepy.error.RateLimitError:
			sleep(10)
		except tweepy.error.TweepError:
			return (0, 1)
	return(user.followers_count, user.friends_count)