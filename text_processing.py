import string
import nltk
nltk.download('stopwords')

#splits a string into a list of tokens
def tokenize_text(text):
	return text.split()

#gets text from a tweet JSON object
def get_text(tweet):
	return tweet['text']

#removes symbols from a list of tokens
def remove_symbols(tokens):
	whitelist = make_set(string.ascii_letters)
	new_tokens = []
	for token in tokens:
		new_tokens.append(''.join(char for char in token if char in whitelist))
		#remove the token, if the result of pruning is an empty symbol
		if new_tokens[-1] == '':
			new_tokens.pop(-1)
	return new_tokens

#turns the letters of a token list into lowercase
def lowercase_tokens(tokens):
	new_tokens = []
	#keeping a dict of uppercase letters, for fast checking O(1)!!!!!!!!!
	upper_dict = make_dict(string.ascii_uppercase)
	lower_letters = string.ascii_lowercase
	for token in tokens:
		new_token = ""
		for char in token:
			if char in upper_dict:
				new_token += lower_letters[upper_dict[char]]
			else:
				new_token += char
		new_tokens.append(new_token)
	return new_tokens

#removes stopwords from a list of tokens (use after turning into lowercase and removing symbols)
def remove_stopwords(tokens):
	new_tokens = []
	stop_words = set(remove_symbols(nltk.corpus.stopwords.words("english")))
	for token in tokens:
		if not token in stop_words:
			new_tokens.append(token)
	return new_tokens

#turns a string into a set, for faster checking if elements are in it or not
def make_set(string):
	set_string = set()
	for char in string:
		set_string.add(char)
	return set_string

#turns a string into a dict, saving the indices of each character (for example 'ABC' -> {'A'=0, 'B'=1, 'C'=2})
def make_dict(string):
	dict_string = {}
	i = 0
	for char in string:
		dict_string[char] = i
		i += 1
	return dict_string

#tokenizes, removes symbols and then lowercases one specific word
def process_keyword(keyword):
	return lowercase_tokens(remove_symbols(tokenize_text(keyword)))

#removes the keywords from a list of tokens
def remove_keyword(tokens, keyword):
	key_set = set(keyword)
	new_tokens = []
	for token in tokens:
		if not token in key_set:
			new_tokens.append(token)
	return new_tokens

#prepares a tweet for the sentiment analysis
def prepare_tweet(tweet, keyword):
	"""text = get_text(tweet)
	tokens = tokenize_text(text)
	no_symbols = remove_symbols(tokens)
	prepared_keyword = process_keyword(keyword)
	lowercase = lowercase_tokens(no_symbols)
	no_stopwords = remove_stopwords(lowercase, prepared_keyword)"""
	#compressed dat shit into one line!
	return remove_keyword(remove_stopwords(lowercase_tokens(remove_symbols(tokenize_text(get_text(tweet))))), process_keyword(keyword))

#same as above, but doesn't remove stop words, only removes the keyword
def prepare_tweet_include_stopwords(tweet, keyword):
	return remove_keyword(lowercase_tokens(remove_symbols(tokenize_text(get_text(tweet)))), process_keyword(keyword))