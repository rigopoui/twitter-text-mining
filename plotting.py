import matplotlib.pyplot as plt
import numpy as np
import matplotlib.patches as mpatches
#import sklearn.preprocessing as nrm
from scipy import special
import pathlib


#Plots top 50 using a tuple which holds all frequencies of tweeters words sorted for each trend.
#with_sh is a bool which indicates whether we are using the tuple with or without stopwords
#trends is a list with the trends
#Plot saves localy the plot. 
def plot_top_50(frequencies_sorted,trends,with_sw):
	top50words=[]
	frequencyNum=[]
	xAxis=[]
	#Iteration of frequencies to extract words and Frequency of them for every trend.
	for i in range(len(frequencies_sorted)):
		top50words.clear()
		frequencyNum.clear()
		xAxis.clear()
		fig, ax = plt.subplots()
		for j in range(50):
			try: 
				top50words.append(frequencies_sorted[i][j][0])
				frequencyNum.append(frequencies_sorted[i][j][1])
				xAxis.append(j)
			except IndexError:
				break
		#Adding labels,title for plot
		filename = 'images/trend_'+str(i)+'/top_50.png'
		ax.bar(xAxis,frequencyNum)
		plt.xticks(xAxis,top50words, rotation='90')		
		if  not with_sw:
			ax.set(xlabel='words',ylabel='frequency of words',title='Word frequency histogram for trend: ' + trends[i])
		else:
			ax.set(xlabel='words',ylabel='frequency of words',title='Word frequency histogram with stopwords for trend: ' + trends[i])			
			filename = 'images/trend_'+str(i)+'/top_50_with_sw.png'
		#Adjust the space between words and makes the plot bigger	
		plt.gca().margins(x=0)
		plt.gcf().canvas.draw()
		tl = plt.gca().get_xticklabels()
		maxsize = max([t.get_window_extent().width for t in tl])
		m = 0.2 # inch margin
		s = maxsize/plt.gcf().dpi*len(xAxis)+2*m
		margin = m/plt.gcf().get_size_inches()[0] + 0.1
		plt.gcf().subplots_adjust(left=margin, right=1.-margin)
		plt.gcf().set_size_inches(s, plt.gcf().get_size_inches()[1])
		plt.tight_layout()
		#Saving
		plt.savefig(filename, dpi=None, facecolor='w', edgecolor='w',
        orientation='portrait', papertype=None, format='png',
        transparent=False, bbox_inches=None, pad_inches=0.1,
        frameon=None)

#Again we use frequncies sorted to extract the information we need.
def zipf(frequencies_sorted,trends):
	#Iteration of frequencies_sorted to extract frequencies for every trend.
	for i in range(len(frequencies_sorted)):
		frequencyNum=[]	
		xAxis=[]
		words=[]
		a= 2.
		for j in range(len(frequencies_sorted[i])):
			frequencyNum.append(frequencies_sorted[i][j][1]/(j+1))
			words.append(frequencies_sorted[i][j][0])
			xAxis.append(j)
		#Adding details to plot		
		filename = 'images/trend_'+str(i)+'/zipf.png'  
		fig, ax = plt.subplots()
		ax.set(xlabel='word rank',ylabel='word frequency / word rank',title='Zipf diagram for trend: ' + trends[i])    
		ax.loglog(xAxis,frequencyNum,linewidth =2,color='blue')
		#Saving
		plt.savefig(filename, dpi=None, facecolor='w', edgecolor='w',
        orientation='portrait', papertype=None, format='png',
        transparent=False, bbox_inches=None, pad_inches=0.1,
        frameon=None)	
	plt.close()	

#We are using sentiments to extract the ratios for each tweet in each trend.	
def pie(sentiments,trends):
	x=[]
	labels = 'Positive' ,'Negative','Neutral'
	#Iteration of sentiments.
	for i in range(len(sentiments)):
		j = 0
		filename = 'images/trend_'+str(i)+'/pie.png'
		x.clear()
		#Adding info to plots
		explode = (0, 0, 0, 0)
		x.append(sentiments[i]['positive_probability'])
		x.append(sentiments[i]['negative_probability'])
		x.append(sentiments[i]['neutral_probability'])
		#mathematical magic
		pie_x = [x[0]*((1 - x[2])/(x[0]+x[1])), x[1]*((1 - x[2])/(x[0]+x[1])), x[2]]
		fig, ax = plt.subplots()
		ax.axis("equal")
		ax.set(xlabel='',ylabel='',title='Sentiment distribution for trend: ' + trends[i])
		def sentiment_values(val):
			nonlocal j
			nonlocal x
			item = x[j]*100
			item = str(item)[0:5]+'%'
			j+=1 
			return item
		plt.pie(pie_x, labels=labels, autopct=sentiment_values, shadow=False)
		#Saving 
		plt.savefig(filename, dpi=None, facecolor='w', edgecolor='w',
        orientation='portrait', papertype=None, format='png',
        transparent=False, bbox_inches=None, pad_inches=0.1,
        frameon=None)
	plt.close()		



def cdf(users_data,trends):
	x=[]
	y=[]
	k = 0
	sum = 0
	#Iteration of  users_data to get followers/friends.
	for j in range(len(trends)):
		for i in range(len(users_data[j])):
			y.append(users_data[j][i]['followers_friends_ratio'])
			x.append(k + 1)
			k += 1
			sum += users_data[j][i]['followers_friends_ratio'] 
	#Adding to plot		
	fig, ax = plt.subplots()
	ax.set(xlabel='Number of users',ylabel='cumulative frequency of followers / friends ratio',title=' Cumulative Distribution Frequency')
	tempY = normalizing(y,sum)				
	tempY = np.cumsum(tempY)
	plt.plot(x,tempY)
	#Saving
	plt.savefig('images/user_ratio_cdf.png', dpi=None, facecolor='w', edgecolor='w',
    orientation='portrait', papertype=None, format='png',
    transparent=False, bbox_inches=None, pad_inches=0.1 ,
    frameon=None)
	plt.close()	

def normalizing(table,sum):
	temp=[]
	for i in range(len(table)):
		temp.append(table[i]/sum)
	return temp	
#Calling all plots
def gotta_plot_them_all(frequencies,frequencies_with_stopwords,sentiments,users_data,trends):
	make_dirs(len(trends))
	plot_top_50(frequencies,trends,False)
	plot_top_50(frequencies_with_stopwords,trends,True)
	zipf(frequencies,trends)
	pie(sentiments,trends)
	cdf(users_data,trends)

#creates folders for the plot files
def make_dirs(trend_size):
	for i in range(trend_size):
		pathlib.Path('images/trend_'+str(i)).mkdir(parents=True, exist_ok=True)
