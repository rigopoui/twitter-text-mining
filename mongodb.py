from pymongo import MongoClient
from pymongo import ReturnDocument
from bson.son import SON

#connect to the database
client = MongoClient()
twitter_database = client.twitter_database

#saves the tweets of one collection/trend
def save_tweets(tweets, trend_index):
	twitter_database["tweets_"+str(trend_index)].insert_many(tweets)

#gets all tweets from the database - mostly for plot mode
def get_all_tweets(index):
	tweets = []
	for i in range(index):
		tweets.append([])
		cursor = twitter_database["tweets_"+str(i)].find()
		for tweet in cursor:
			tweets[i].append(tweet)
	return tweets

#gets all user data from the database - for plot mode
def get_user_data(index):
	users_data = []
	for i in range(index):
		users_data.append([])
		cursor = twitter_database["users_"+str(i)].find()
		for trend_user in cursor:
			users_data[i].append(trend_user)
	return users_data

#gets sentiment from a tweet in the database - for plot mode
def get_sentiment(i, tweet_id):
	tweet = twitter_database["tweets_"+str(i)].find_one({'id': tweet_id})
	sentiment = {"probability": {"neg": tweet["negative_probability"],"neutral": tweet["neutral_probability"],"pos": tweet["positive_probability"]},"label": tweet["label"]}
	return sentiment

#converts a dict of trends back into a list, for usage in python
def trends_to_list():
	trends = []
	dict_trends = twitter_database.trends_collection.find_one()
	for i in range(0, len(dict_trends)-1):
		trends.append(dict_trends["trend_"+str(i)])
	return trends

#returns the place stored in the database
def get_place():
	return twitter_database.place_collection.find_one()['place']

#convers an array of trends into a dict, so it can easily be saved in mongodb
def trends_to_dict(trends):
	dict_trends={}
	for i in range(0,len(trends)):
		dict_trends["trend_"+str(i)]=trends[i]
	return dict_trends

#checks if the database is empty
def check_empty():
	if len(twitter_database.collection_names()) == 0:
		return True
	else:
		return False

#initializes the database - deletes it then stores the trends and the place
def init_database(place, trends):
	client.drop_database('twitter_database')	
	twitter_database.trends_collection.insert_one(trends_to_dict(trends))
	twitter_database.place_collection.insert_one({'place': place})

#saves the sentiment for each tweet(gonna take some time with 1500 tweets)
def save_sentiment(i, tweet_id, sentiment):
	return twitter_database["tweets_"+str(i)].find_one_and_update({'id': tweet_id}, 
		{'$set': {
			"label": sentiment["label"], 
			"positive_probability" : sentiment["probability"]["pos"], 
			"neutral_probability" : sentiment["probability"]["neutral"], 
			"negative_probability" : sentiment["probability"]["neg"]}}, 
		return_document=ReturnDocument.AFTER)

#returns for one trend, a cursor which when iterated, gives a dict containing the user id, and the sentiments of the tweet, grouped by user ID
def group_sentiments_by_user(trend_index):
	pipeline = [
		{"$group": {"_id": { "user_id" : "$user.id", "pos" : "$positive_probability", "neg" : "$negative_probability", "neutral" : "$neutral_probability"}}},
		{"$sort": SON([("_id", -1)])}
	]
	return twitter_database["tweets_"+str(trend_index)].aggregate(pipeline)

#saves the user data for one collection/trend
def save_user_data(data, trend_index):
	twitter_database["users_"+str(trend_index)].insert_many(data)